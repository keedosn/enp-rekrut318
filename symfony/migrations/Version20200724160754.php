<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20200724160754 extends AbstractMigration
{

    public function up(Schema $schema) : void
    {
        $this->addSql("
            CREATE TABLE customer (
                id INT AUTO_INCREMENT NOT NULL, 
                name VARCHAR(255) NOT NULL, 
                PRIMARY KEY(id)
            )
            DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB;
        ");

        $this->addSql("
            CREATE TABLE cart (
                id INT AUTO_INCREMENT NOT NULL, 
                customer_id INT DEFAULT NULL, 
                UNIQUE INDEX UNIQ_BA388B79395C3F3 (customer_id), 
                PRIMARY KEY(id)
            )
            DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB;
        ");

        $this->addSql("
            CREATE TABLE cart_item (
                id INT AUTO_INCREMENT NOT NULL, 
                cart_id INT NOT NULL, 
                price NUMERIC(10, 0) NOT NULL, 
                name VARCHAR(255) NOT NULL, 
                INDEX IDX_F0FE25271AD5CDBF (cart_id), 
                PRIMARY KEY(id)
            )
            DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB;
        ");

        $this->addSql("
            CREATE TABLE product (
                id INT AUTO_INCREMENT NOT NULL, 
                name VARCHAR(255) NOT NULL, 
                price NUMERIC(10, 0) NOT NULL, 
                PRIMARY KEY(id)
            )
            DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB;
        ");

        $this->addSql("
            ALTER TABLE cart 
                ADD CONSTRAINT FK_BA388B79395C3F3 FOREIGN KEY (customer_id) REFERENCES customer (id);
        ");
        $this->addSql("
            ALTER TABLE cart_item 
                ADD CONSTRAINT FK_F0FE25271AD5CDBF FOREIGN KEY (cart_id) REFERENCES cart (id);
        ");
    }

    public function down(Schema $schema) : void
    {
        $this->addSql("DRP TABLE product;");
        $this->addSql("DRP TABLE customer;");
        $this->addSql("DRP TABLE cart_item;");
        $this->addSql("DRP TABLE cart;");
    }
}
