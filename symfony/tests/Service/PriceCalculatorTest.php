<?php

declare(strict_types=1);

namespace App\Tests\Service;

use App\Service\PriceCalculator;
use App\Tests\FixturesTestCase;

class PriceCalculatorTest extends FixturesTestCase
{
    public function testZeroPriceShouldReturnZeroPriceAfterCalculations()
    {
        $calculator = new PriceCalculator($priceModifier = 100);
        $newPrice = $calculator->calculate(0);

        $this->assertEquals(0, $newPrice);
    }

    public function testZeroPriceShouldReturnZeroPriceAfterCalculationsWith50PercentPriceModifier()
    {
        $calculator = new PriceCalculator($priceModifier = 50);
        $newPrice = $calculator->calculate(0);

        $this->assertEquals(0, $newPrice);
    }

    public function test100PriceShouldReturnCorrectlyModifiedPriceAfterCalculations()
    {
        $calculator = new PriceCalculator($priceModifier = 100);
        $newPrice = $calculator->calculate(100);

        $this->assertEquals(100, $newPrice);
    }

    public function test100PriceShouldReturnCorrectlyModifiedPriceAfterCalculationsWith200PercentPriceModifier()
    {
        $calculator = new PriceCalculator($priceModifier = 200);
        $newPrice = $calculator->calculate(100);

        $this->assertEquals(200, $newPrice);
    }

    public function test100PriceShouldReturnCorrectlyModifiedPriceAfterCalculationsWith50PercentPriceModifier()
    {
        $calculator = new PriceCalculator($priceModifier = 50);
        $newPrice = $calculator->calculate(100);

        $this->assertEquals(50, $newPrice);
    }
}
