<?php

declare(strict_types=1);

namespace App\Tests\Service;

use App\Entity\Cart;
use App\Service\CartService;
use App\Tests\FixturesTestCase;

class CartServiceTest extends FixturesTestCase
{
    public function testEmptyCartShoulReturnTotalEqualsZeroValue()
    {
        $cart = new Cart();
        $cartService = CartService::create($cart);

        $this->assertEquals(0.0, $cartService->getTotal());
    }

    public function testCustomer1CartShoulReturnProperTotalValue()
    {
        $customer = $this->getFixtures()->getFixtureObject('customer_1');
        $cart = static::$container->get('doctrine')->getRepository(Cart::class)->findOneForCustomer($customer);
        $cartService = CartService::create($cart);

        $this->assertEquals(300.0, $cartService->getTotal());
    }

    public function testCustomer2CartShoulReturnProperTotalValue()
    {
        $customer = $this->getFixtures()->getFixtureObject('customer_2');
        $cart = static::$container->get('doctrine')->getRepository(Cart::class)->findOneForCustomer($customer);
        $cartService = CartService::create($cart);

        $this->assertEquals(100.0, $cartService->getTotal());
    }
}
