<?php

declare(strict_types=1);

namespace App\Tests\Service\UseCase;

use App\Entity\Cart;
use App\Entity\Customer;
use App\Service\UseCase\Cart\GetCart;
use App\Tests\FixturesTestCase;

class GetCartTest extends FixturesTestCase
{
    public function testReturnProperCartForCustomer()
    {
        $cartRepo = static::$container->get('doctrine')->getRepository(Cart::class);

        $customer1 = $this->getFixtures()->getFixtureObject('customer_1');
        $customer2 = $this->getFixtures()->getFixtureObject('customer_2');
        $getCart = new GetCart($cartRepo);
        $cartForCustomer1 = $getCart->get($customer1);
        $cartForCustomer2 = $getCart->get($customer2);

        $this->assertEquals($cartForCustomer1->getCustomer()->getName(), 'Pani Bożenka z obsługi klienta 1');
        $this->assertEquals($cartForCustomer2->getCustomer()->getName(), 'Pani Bożenka z obsługi klienta 2');
    }

    // public function testZeroValueCustomer()
    // {
    //     $cartRepo = static::$container->get('doctrine')->getRepository(Cart::class);
    //     $getCart = new GetCart($cartRepo);
    //     $customer = $this->getFixtures()->getFixtureObject('customer_3');
    //     $cart = $getCart->get($customer);

    //     $this->assertEquals($cart->getCustomer()->getName(), '');
    // }
}
