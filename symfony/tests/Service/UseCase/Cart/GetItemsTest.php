<?php

declare(strict_types=1);

namespace App\Tests\Service\UseCase;

use App\Entity\CartItem;
use App\Service\UseCase\Cart\GetItems;
use App\Tests\FixturesTestCase;

class GetItemsTest extends FixturesTestCase
{
    public function testEmptyCustomerCartShouldReturnEmptyItemsList()
    {
        $cart = $this->getFixtures()->getFixtureObject('empty_cart');
        $cartItemRepo = static::$container->get('doctrine')->getRepository(CartItem::class);
        $getItems = new GetItems($cartItemRepo);

        $this->assertEquals(count($getItems->get($cart)), 0);
    }

    public function testCustomer1CartShouldReturnProperItemsList()
    {
        $cart = $this->getFixtures()->getFixtureObject('cart_for_customer1');
        $cartItemRepo = static::$container->get('doctrine')->getRepository(CartItem::class);
        $getItems = new GetItems($cartItemRepo);

        $this->assertEquals(count($getItems->get($cart)), 2);
    }

    public function testCustomer2CartShouldReturnProperItemsList()
    {
        $cart = $this->getFixtures()->getFixtureObject('cart_for_customer2');
        $cartItemRepo = static::$container->get('doctrine')->getRepository(CartItem::class);
        $getItems = new GetItems($cartItemRepo);

        $this->assertEquals(count($getItems->get($cart)), 1);
    }
}
