<?php

declare(strict_types=1);

namespace App\Tests;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

abstract class FixturesTestCase extends KernelTestCase
{
    private static EntityManagerInterface $entityManager;
    private static Fixtures $fixtures;

    const USER_SPECIALIST_1_ACCESS_TOKEN = 'user_specialist_1_access_token';
    const USER_ADMIN_1_ACCESS_TOKEN = 'user_admin_1_access_token';

    public static function setUpBeforeClass(): void
    {
        $kernel = static::bootKernel();

        static::$entityManager = $kernel->getContainer()->get('doctrine.orm.entity_manager');
        static::$entityManager->beginTransaction();
        static::$entityManager->getConnection()->setAutoCommit(false);

        static::$fixtures = Fixtures::getFixturesInstance();
    }

    protected function setUp(): void
    {
    }

    protected function tearDown(): void
    {
        if (static::$entityManager->getConnection()->isTransactionActive()) {
            static::$entityManager->rollback();
        }
    }

    public function getFixtures(): Fixtures
    {
        return self::$fixtures;
    }
}
