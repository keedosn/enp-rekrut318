<?php
declare(strict_types=1);

namespace spec\App\Service;

use App\Entity\Cart;
use App\Entity\CartItem;
use App\Entity\Customer;
use App\Infrastructure\CartInterface;
use App\Service\CartService;
use PhpSpec\ObjectBehavior;

/**
 * @author Artur Świerc <artur.swierc@enp.pl>
 */
class CartServiceSpec extends ObjectBehavior
{
    public function it_is_initializable()
    {
        $cart = $this->getCart();
        $this->beConstructedThrough('create', [$cart]);

        $this->shouldHaveType(CartService::class);
        $this->shouldImplement(CartInterface::class);
    }

    // public function it_can_add_item()
    // {
    //     $cart = $this->getCart();
    //     $item = $this->getCartItems()[0];

    //     $this->beConstructedThrough('create', [$cart]);
    //     $this->addItem($item);
    //     dump($this->getTotal());exit;
    //     $this->getTotal()->willReturn(100);
    // }

    // public function it_can_get_correct_total_price()
    // {
    //     $cart = $this->getCart();
    //     $this->beConstructedThrough('create', [$cart]);

    //     $item = $cart->getItems()[0];
    //     $item->getPrice()->willReturn(100);
    //     $item->getName()->willReturn('TV LCD');

    //     $item2 = $cart->getItems()[1];
    //     $item2->getPrice()->willReturn(200);
    //     $item2->getName()->willReturn('Samsung S1000');

    //     $this->addItem($item);
    //     $this->addItem($item2);

    //     $this->getTotal()->shouldReturn(300);
    // }

    // public function it_can_change_total_price()
    // {
    //     $cart = $this->getCart();
    //     $this->beConstructedThrough('create', [$cart]);

    //     $item = $cart->getItems()[0];
    //     $item->getPrice()->willReturn(100);
    //     $item->getName()->willReturn('TV LCD');

    //     $this->addItem($item);
    //     $this->setTotal(200);

    //     $this->getTotal()->shouldReturn(200);
    // }

    private function getCart(): Cart
    {
        $customer = new Customer();
        $customer->setName('Pani bożenka');

        $cartItems = $this->getCartItems();

        $cart = new Cart();
        $cart->setCustomer($customer);
        $cart->addItem($cartItems[0]);
        $cart->addItem($cartItems[1]);

        return $cart;
    }

    private function getCartItems(): array
    {
        return [
            (new CartItem())
            ->setName('TV LCD')
            ->setPrice(100),
            (new CartItem())
            ->setName('Samsung S1000')
            ->setPrice(200)
        ];
    }
}
