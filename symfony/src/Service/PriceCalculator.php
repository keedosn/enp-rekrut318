<?php

declare(strict_types=1);

namespace App\Service;

class PriceCalculator
{
    private int $priceModifier;

    public function __construct(int $priceModifier)
    {
        $this->priceModifier = $priceModifier;
    }

    public function calculate(float $price): float
    {
        if ($this->priceModifier > 0) {
            $modifier = ($this->priceModifier / 100);

            return $price * $modifier;
        }

        return $price;
    }
}
