<?php

declare(strict_types=1);

namespace App\Service\UseCase\Cart;

use App\Entity\Cart;
use App\Entity\CartItem;
use App\Repository\CartItemRepository;

class GetItems
{
    private CartItemRepository $cartItemRepository;

    public function __construct(CartItemRepository $cartItemRepository)
    {
        $this->cartItemRepository = $cartItemRepository;
    }

    /**
     * @return array<CartItem>
     */
    public function get(Cart $cart): array
    {
        return $this->cartItemRepository->findAllForCart($cart);
    }
}
