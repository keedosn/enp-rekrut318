<?php

declare(strict_types=1);

namespace App\Service\UseCase\Cart;

use App\Entity\Cart;
use App\Entity\Customer;
use App\Repository\CartRepository;

class GetCart
{
    private CartRepository $cartRepository;

    public function __construct(CartRepository $cartRepository)
    {
        $this->cartRepository = $cartRepository;
    }

    public function get(Customer $customer): Cart
    {
        return $this->cartRepository->findOneForCustomer($customer);
    }
}
