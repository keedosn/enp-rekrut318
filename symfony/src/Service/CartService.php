<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Cart;
use App\Entity\CartItem;
use App\Infrastructure\CartInterface;

/**
 * @author Artur Świerc <artur.swierc@enp.pl>
 */
class CartService implements CartInterface
{
    private Cart $cart;

    private float $total = 0;

    public static function create(Cart $cart): CartService
    {
        return new self($cart);
    }

    public function __construct(Cart $cart)
    {
        $this->cart = $cart;

        foreach ($cart->getItems() as $item) {
            $this->addItem($item);
        }
    }

    public function addItem(CartItem $item): void
    {
        $this->cart->addItem($item);
        $this->total += $item->getPrice();
    }

    public function getTotal(): float
    {
        return $this->total;
    }

    public function setTotal(float $total): void
    {
        $this->total = $total;
    }
}
