<?php

declare(strict_types=1);

namespace App\Infrastructure;

interface Buyable
{
    public function setPrice(float $price): Buyable;

    public function getPrice(): float;
}
