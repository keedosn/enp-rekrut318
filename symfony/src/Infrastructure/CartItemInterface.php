<?php

declare(strict_types=1);

namespace App\Infrastructure;

/**
 * @author Artur Świerc <artur.swierc@enp.pl>
 */
interface CartItemInterface
{
    public function setPrice(float $price): CartItemInterface;

    public function getPrice(): float;

    public function setName(string $name): CartItemInterface;

    public function getName(): string;
}
