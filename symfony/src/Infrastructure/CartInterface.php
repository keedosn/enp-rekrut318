<?php

declare(strict_types=1);

namespace App\Infrastructure;

use App\Entity\CartItem;

/**
 * @author Artur Świerc <artur.swierc@enp.pl>
 */
interface CartInterface
{
    public function addItem(CartItem $item): void;

    public function getTotal(): float;

    public function setTotal(float $total): void;
}
