<?php

declare(strict_types=1);

namespace App\Entity;

use App\Infrastructure\Buyable;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Product implements Buyable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string")
     */
    private string $name;

    /**
     * @ORM\Column(type="decimal")
     */
    private float $price;

    public function getId(): int
    {
        return $this->id;
    }

    public function setPrice(float $price): Product
    {
        $this->price = $price;

        return $this;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function setName(string $name): Product
    {
        $this->name = $name;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }
}
