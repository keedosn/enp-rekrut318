<?php

declare(strict_types=1);

namespace App\Entity;

use App\Infrastructure\CartItemInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CartItemRepository")
 */
class CartItem implements CartItemInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\ManyToOne(targetEntity="Cart", inversedBy="items")
     * @ORM\JoinColumn(name="cart_id", referencedColumnName="id", nullable=false)
     */
    private Cart $cart;

    /**
     * @ORM\Column(type="decimal")
     */
    private float $price;

    /**
     * @ORM\Column(type="string")
     */
    private string $name;

    public function getId(): int
    {
        return $this->id;
    }

    public function setCart(Cart $cart): CartItem
    {
        $this->cart = $cart;

        return $this;
    }

    public function getCart(): Cart
    {
        return $this->cart;
    }

    public function setPrice(float $price): CartItem
    {
        $this->price = $price;

        return $this;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function setName(string $name): CartItem
    {
        $this->name = $name;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }
}
