<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CartRepository")
 */
class Cart
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\OneToOne(targetEntity="Customer")
     * @ORM\JoinColumn(name="customer_id", referencedColumnName="id")
     */
    private Customer $customer;

    /**
     * @ORM\OneToMany(targetEntity="CartItem", mappedBy="cart")
     */
    private Collection $items;

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setCustomer(Customer $customer): Cart
    {
        $this->customer = $customer;

        return $this;
    }

    public function getCustomer(): Customer
    {
        return $this->customer;
    }

    public function addItem(CartItem $cartItem): void
    {
        $this->items->add($cartItem);
    }

    public function removeItem(CartItem $cartItem): void
    {
        if ($this->items->contains($cartItem)) {
            $this->items->removeElement($cartItem);
        }
    }

    public function getItems(): Collection
    {
        return $this->items;
    }
}
