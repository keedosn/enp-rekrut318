<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Customer;
use App\Service\CartService;
use App\Service\PriceCalculator;
use App\Service\UseCase\Cart\GetCart;
use App\Service\UseCase\Cart\GetItems;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class OperatorController extends AbstractController
{
    /**
     * @Route("/", methods="GET")
     */
    public function indexAction(GetCart $getCart, PriceCalculator $priceCalculator): Response
    {
        $customer = $this->get('doctrine')->getRepository(Customer::class)->findOneBy([], ['id' => 'ASC']);
        $cart = $getCart->get($customer);
        $cartService = CartService::create($cart);

        $oldTotal = $cartService->getTotal();
        $newTotal = $priceCalculator->calculate($oldTotal);

        return $this->render('operator/index.html.twig', [
            'oldTotal' => $oldTotal,
            'newTotal' => $newTotal,
        ]);
    }

    /**
     * @Route("/cart", methods="GET")
     */
    public function cartAction(GetCart $getCart, GetItems $getItems, PriceCalculator $priceCalculator): Response
    {
        $customer = $this->get('doctrine')->getRepository(Customer::class)->findOneBy([], ['id' => 'ASC']);
        $cart = $getCart->get($customer);
        $cartService = CartService::create($cart);
        $cartItems = $getItems->get($cart);

        $oldTotal = $cartService->getTotal();
        $total = $priceCalculator->calculate($oldTotal);

        return $this->render('operator/cart.html.twig', [
            'total' => $total,
            'items' => $cartItems,
        ]);
    }
}
