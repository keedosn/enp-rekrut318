.DEFAULT_GOAL := start

# Symfony commands
cc:
	docker-compose exec php sh -c "bin/console c:c"

cw:
	docker-compose exec php sh -c "bin/console c:w"

# Testing
test-unit:
	docker-compose exec php sh -c "vendor/bin/simple-phpunit"

test-spec:
	docker-compose exec php sh -c "vendor/bin/phpspec run"

# Coding standars
phpcs:
	docker-compose exec php sh -c "vendor/bin/php-cs-fixer fix --allow-risky=yes"

phpcs-dry:
	docker-compose exec php sh -c "vendor/bin/php-cs-fixer fix --allow-risky=yes --dry-run --verbose --show-progress=dots"

phpstan:
	docker-compose exec php sh -c "vendor/bin/phpstan analyse src"

# Container
start:
	docker-compose up -d --force-recreate

stop:
	docker-compose down

build:
	docker-compose build

shell:
	docker-compose exec php sh

clear:
	docker volume prune -f
