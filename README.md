# Installation

To start project copy .env.dist file in symfony/ folder and name it .env, next step is to edit it witch your preffered credentials, after that just enter following command in console:

```sh
$ make
```

After build project will be accessibly in  your browser at [address](http://localhost/).

## To run tests and cs checker run following commands in your console:

### To run unit tests

```sh
$ make test-unit
```

### To run phpspec tests

```sh
$ make test-spec
```

### Checking coding standards

```sh
$ make phpstan
$ make phpcs-dry
```
