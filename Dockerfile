ARG PHP_IMAGE=php:7.4-fpm-alpine
ARG NGINX_IMAGE=nginx:1.19-alpine

# PHP Stage
FROM ${PHP_IMAGE} AS php

COPY ./docker/build/php/ /etc/php7/

ARG APP_DIR=/var/www/app
WORKDIR ${APP_DIR}

# Install base packages
RUN apk add --update --no-cache coreutils acl php7-fpm php7-cli php7-iconv php7-json \
   php7-intl php7-zlib php7-phar php7-zip make curl

RUN docker-php-ext-install pdo pdo_mysql

# Install Composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer
ENV COMPOSER_ALLOW_SUPERUSER=1

# RUN setfacl -dR -m u:www-data:rwX -m u:root:rwX ${APP_DIR}/var/cache ${APP_DIR}/var/log
# RUN setfacl -R -m u:www-data:rwX -m u:root:rwX ${APP_DIR}/var/cache ${APP_DIR}/var/log
# RUN chmod -R 777 ${APP_DIR}/var/cache ${APP_DIR}/var/log

# Entrypoint
COPY ./docker/docker-entrypoint.sh /usr/local/bin/
RUN chmod ug+x /usr/local/bin/docker-entrypoint.sh
ENTRYPOINT ["docker-entrypoint.sh"]

CMD ["php-fpm"]

# Nginx Stage
FROM ${NGINX_IMAGE} AS nginx

COPY ./docker/build/nginx/conf.d/ /etc/nginx/conf.d/
COPY ./docker/build/nginx/ /etc/nginx/

CMD ["nginx", "-g", "daemon off;"]

EXPOSE 80