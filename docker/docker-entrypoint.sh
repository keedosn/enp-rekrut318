#!/bin/sh

echo "Initialize project"

# Install composer packages
if [ "$APP_ENV" != 'prod' ]; then
    composer install --prefer-dist --no-progress --no-suggest --no-interaction
fi

# Cache clean & warmup
${APP_DIR}/bin/console c:c
${APP_DIR}/bin/console c:w

echo "Waiting for db to be ready..."
until ${APP_DIR}/bin/console doctrine:query:sql "SELECT 1" > /dev/null 2>&1; do
    sleep 1
done

${APP_DIR}/bin/console d:d:c --if-not-exists
${APP_DIR}/bin/console d:m:m -n --allow-no-migration
${APP_DIR}/bin/console h:f:l -n

echo "Setup complete"

exec "$@"
